xmountains (2.10-1) unstable; urgency=low

  * New upstream release:
    + Dropped gcc10 FTBFS patch, merged upstream.
  * Fixed debian/watch file.
  * Removed debian/*.man, merged upstream.
  * Removed debian/menu file.
  * debian/control:
    + Updated Standards-Version to 4.5.1

 -- Kartik Mistry <kartik@debian.org>  Tue, 26 Jan 2021 18:11:08 +0530

xmountains (2.9-7) unstable; urgency=low

  * Added debian/gitlab-ci.yml.
  * debian/control:
    + Switched to debhelper-compat.
    + Updated Standards-Version to 4.5.0
    + Added 'Rules-Requires-Root' field.
  * Added debian/upstream/metadata file.
  * Added patch to fix FTBFS with gcc10 from upstream (Closes: #957988)

 -- Kartik Mistry <kartik@debian.org>  Fri, 24 Jul 2020 10:25:20 +0530

xmountains (2.9-6) unstable; urgency=low

  * debian/control:
    + Fixed Vcs-Git URL.
    + Updated Homepage.
    + Updated Standards-Version to 4.2.1

 -- Kartik Mistry <kartik@debian.org>  Thu, 29 Nov 2018 08:36:30 +0530

xmountains (2.9-5) unstable; urgency=low

  * debian/rules:
    + Use dh.
  * debian/control:
    + Build-Dep: Removed cdbs, updated debhelper >= 11.
    + Updated Standards-Version to 4.1.3.
    + Updated Vcs-* URLs.
  * Set debian/compat to 11.
  * debian/copyright:
    + Updated upstream URL.
    + Use https.
  * Added desktop file (Closes: #738071)

 -- Kartik Mistry <kartik@debian.org>  Tue, 23 Jan 2018 15:12:59 +0530

xmountains (2.9-4) unstable; urgency=low

  * debian/control:
    + Updated Standards-Version to 3.9.8
    + Fixed Vcs-* URLs.
    + Removed dependency on hardening-wrapper.
  * debian/rules:
    + Added dpkg-buildflags support.
  * Fixed debian/copyright.

 -- Kartik Mistry <kartik@debian.org>  Fri, 08 Jul 2016 12:02:05 +0530

xmountains (2.9-3) unstable; urgency=low

  * debian/control:
    + Updated Standards-Version to 3.9.4
    + Added VCS-* URLs.
  * debian/copyright:
    + Moved to copyright-format 1.0
  * Added hardening support.

 -- Kartik Mistry <kartik@debian.org>  Tue, 15 Oct 2013 08:41:19 +0530

xmountains (2.9-2) unstable; urgency=low

  * Upload to unstable

 -- Kartik Mistry <kartik@debian.org>  Sat, 12 Feb 2011 14:13:01 +0530

xmountains (2.9-1) experimental; urgency=low

  * New upstream release
  * debian/control:
    + Updated Standards-Version to 3.9.1 (no changes needed)

 -- Kartik Mistry <kartik@debian.org>  Mon, 20 Dec 2010 14:15:36 +0530

xmountains (2.8-2) unstable; urgency=low

  * debian/control:
    + Updated Standards-Version to 3.8.4 (no changes needed)
    + Bumped debhelper dependency to 7
  * debian/source/format:
    + Added for source format 3.0 (quilt)

 -- Kartik Mistry <kartik@debian.org>  Thu, 27 May 2010 17:19:25 +0530

xmountains (2.8-1) unstable; urgency=low

  * New upstream release:
    + All debian/patches merged with upstream
  * debian/control:
    + Updated Standards-Version to 3.8.3

 -- Kartik Mistry <kartik@debian.org>  Fri, 28 Aug 2009 23:44:59 +0530

xmountains (2.7-9) unstable; urgency=low

  * debian/control:
    + Updated Standards-Version to 3.8.1
    + Not using bollin repo now, Removed VCS fields and Torsten Werner
      as uploader
    + Updated my maintainer email address
  * debian/patches/02_spelling_fix.dpatch:
    + [Lintian] Added patch to fix spelling error, sent to upstream
  * debian/copyright:
    + Updated to use correct copyright symbol ©

 -- Kartik Mistry <kartik@debian.org>  Mon, 25 May 2009 10:55:58 +0530

xmountains (2.7-8) unstable; urgency=low

  * Change Build-Depends: xutils-dev. (Closes: #485719)

 -- Torsten Werner <twerner@debian.org>  Wed, 11 Jun 2008 19:35:06 +0200

xmountains (2.7-7) unstable; urgency=low

  [Kartik Mistry]
  * debian/control:
    + Updated Standards-Version to 3.7.3
    + Updated VCS headers
  * debian/install:
    + xmountains will now install in /usr/games
  * debian/menu:
    + Updated xmountains binary file location

 -- Kartik Mistry <kartik.mistry@gmail.com>  Fri, 28 Dec 2007 12:42:14 +0530

xmountains (2.7-6) unstable; urgency=low

  [Kartik Mistry]
  * debian/copyright:
    + Updated upstream author email address
    + Moved copyright out of license section
  * Added debian/watch file
  * debian/control:
    + Moved Homepage to control field

 -- Kartik Mistry <kartik.mistry@gmail.com>  Tue, 27 Nov 2007 14:01:46 +0530

xmountains (2.7-5) unstable; urgency=low

  [Kartik Mistry]
  * Added dpatch support
  * Fixed goofup in previous upload which didn't include previous upstream
    changes, patches now in dpatch format
  * This bug is already fixed, but it was there in 2.7-4 (Closes: #427847)

 -- Kartik Mistry <kartik.mistry@gmail.com>  Thu, 07 Jun 2007 15:03:46 +0530

xmountains (2.7-4) unstable; urgency=low

  [ Kartik Mistry ]
  * New maintainer (Closes: #427396)
  * Fixed build-dependency on xlibs-data (Closes: #419017)
  * Updated to Standards-Version 3.7.2
  * Updated debhelper compability to 5
  * debian/copyright: updated according to standard copyright file, removed
    copyright.header file
  * debian/rules: removed copyright creation from copyright.header
  * debian/control: minor changes in descriptions, added ${misc:Depends}
  * debian/menu: fixed according to standard menu format
  * debian/changelog: removed useless lines from end

  [ Torsten Werner ]
  * Add myself to Uploaders.
  * Add XS-Vcs-* headers.
  * Add homepage to description.

 -- Torsten Werner <twerner@debian.org>  Wed,  6 Jun 2007 19:29:34 +0200

xmountains (2.7-3.1) unstable; urgency=low

  * Non-maintainer upload to do xlibs-dev transition.
  * debian/control: Replaced build-depends on xlibs-dev. (Closes: #346772)

 -- Marc 'HE' Brockschmidt <he@debian.org>  Thu, 19 Jan 2006 16:45:24 +0100

xmountains (2.7-3) unstable; urgency=low

  * Add the French translation of the man page.
  * First release with new maintener. Closes: #307364

 -- Eric Madesclair <eric-m@wanadoo.fr>  Fri,  6 May 2005 13:02:01 +0200

xmountains (2.7-2) unstable; urgency=low

  * fixed lintian errors: binary goes to /usr/bin and menu entries need
  quoting (Thanks to Sergio Talens Oliag)

 -- Miguel Gea Milvaques (Xerako) <correo@miguelgea.com>  Mon, 13 Sep 2004 01:02:01 +0200

xmountains (2.7-1) unstable; urgency=low

  * new upstream version

 -- Miguel Gea Milvaques (Xerako) <correo@miguelgea.com>  Thu,  9 Sep 2004 23:54:21 +0200

xmountains (2.6-9) unstable; urgency=low

  * added #includes and prototypes and various -Wall cleanup, closes: #226692

 -- Torsten Werner <twerner@debian.org>  Thu,  8 Jan 2004 17:03:33 +0000

xmountains (2.6-8) unstable; urgency=low

  * delete Makefile during 'make clean'
  * converted to cdbs

 -- Torsten Werner <twerner@debian.org>  Fri,  2 Jan 2004 21:21:45 +0000

xmountains (2.6-7) unstable; urgency=low

  * adopted package, closes: #202349
  * copied vroot.h from xscreensaver package, closes: #217686

 -- Torsten Werner <twerner@debian.org>  Thu,  1 Jan 2004 17:29:39 +0100

xmountains (2.6-6) unstable; urgency=low

  * orphaned; set maintainer to packages@qa.debian.org

 -- tony mancill <tmancill@debian.org>  Mon, 21 Jul 2003 15:53:12 -0700

xmountains (2.6-5) unstable; urgency=low

  * updated build-depends for sid (closes: #170193)

 -- tony mancill <tmancill@debian.org>  Sat, 23 Nov 2002 14:10:51 -0800

xmountains (2.6-4) unstable; urgency=low

  * a tweak in X_graphics.c for "-w -r 0" by Mark Thomas
  * cosmetic manpage change

 -- tony mancill <tmancill@debian.org>  Thu, 21 Feb 2002 21:18:02 -0800

xmountains (2.6-3) unstable; urgency=low

  * recompiled 2.6 to use VROOT (closes: #134807)

 -- tony mancill <tmancill@debian.org>  Tue, 19 Feb 2002 23:46:44 -0800

xmountains (2.6-2) unstable; urgency=low

  * patches for double-buffering (-w) (in response to #132560)
    (thanks to Mark Thomas: http://www.mindspring.com/~mt31415/)

 -- tony mancill <tmancill@debian.org>  Mon, 18 Feb 2002 09:15:11 -0800

xmountains (2.6-1) unstable; urgency=low

  * new upstream version

 -- tony mancill <tmancill@debian.org>  Sat, 16 Feb 2002 11:13:47 -0800

xmountains (2.5-10) unstable; urgency=low

  * compiled against xfree 4.x libraries for woody release
  * modified menu entry to contain explicit path to xmountains

 -- tony mancill <tmancill@debian.org>  Wed,  5 Dec 2001 20:03:15 -0800

xmountains (2.5-9) unstable; urgency=low

  * new maintainer <tmancill@debian.org> (Closes: bug#69654)

 -- tony mancill <tmancill@debian.org>  Mon, 28 Aug 2000 17:40:14 -0700

xmountains (2.5-8) unstable; urgency=low

  * Build deps.

 -- Joey Hess <joeyh@debian.org>  Sat,  4 Dec 1999 18:32:03 -0800

xmountains (2.5-7) unstable; urgency=low

  * On alpha at least, and probably with some versions of libc 2.1 headers
    in general, "fdim" is a macro. Renamed the fdim variable used by this
    package to xmountains_fdim to prevent collisions.
  * I've chacked on faure and that Closes: #44616 (running xmountains over
    the net to test it sucks :-)

 -- Joey Hess <joeyh@debian.org>  Fri, 10 Sep 1999 15:50:20 -0700

xmountains (2.5-6) unstable; urgency=low

  * FHS

 -- Joey Hess <joeyh@debian.org>  Mon,  6 Sep 1999 18:56:12 -0700

xmountains (2.5-5) unstable; urgency=low

  * Was missing all dependancy info, added missing call to dh_shlibdeps to
    fix.

 -- Joey Hess <joeyh@debian.org>  Thu,  4 Feb 1999 13:33:28 -0800

xmountains (2.5-4) unstable; urgency=low

  * Linked in vroot.h from xscreensaver, which may make xmountains work on
    virtual root windows and fix #28478.

 -- Joey Hess <joeyh@debian.org>  Sat, 24 Oct 1998 18:25:19 -0700

xmountains (2.5-3) unstable; urgency=low

  * Fix minor lintian errors.

 -- Joey Hess <joeyh@debian.org>  Sat, 14 Mar 1998 20:31:40 -0800

xmountains (2.5-2) unstable; urgency=low

  * Updated standards-version.

 -- Joey Hess <joeyh@debian.org>  Mon,  9 Feb 1998 14:13:53 -0800

xmountains (2.5-1) unstable; urgency=low

  * New upstream release (without pristine source since the tar file has no
    subdirectory - sigh..)

 -- Joey Hess <joeyh@debian.org>  Sun, 16 Nov 1997 18:28:19 -0500

xmountains (2.4-5) unstable; urgency=low

  * Corrected copyright file (#14943).
  * Use debhelper.

 -- Joey Hess <joeyh@debian.org>  Sun, 16 Nov 1997 18:07:08 -0500

xmountains (2.4-4) unstable; urgency=low

  * libc6.
  * new format menu file
  * Routine update of debian/rules:
    Fixed binary-indep target.

 -- Joey Hess <joeyh@debian.org>  Mon,  8 Sep 1997 19:26:08 -0400

xmountains (2.4-3) unstable; urgency=low

  * New package standard (and debmake).
  * Added a menu-file.
  * Change of maintainer's address.

 -- Joey Hess <joeyh@debian.org>  Tue, 17 Dec 1996 00:38:23 -0500

2.4.2
* Moved to section games.

2.4-1      Fri Aug 30 19:26:01 EDT 1996
* Initial release
